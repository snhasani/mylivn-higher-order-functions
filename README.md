# web-test-assignment

## Write your own versions of:

- Array.prototype.map
- Array.prototype.filter
- Array.prototype.reduce

In the `src` folder you can find a file related to each higher-order function that asked.

## Local Development

Below is a list of commands you will probably find useful for running tests and
building the source

### `npm run build`

Compiles all `ts` files of the `src` directory into the 'build' directory.

### `npm test`

Runs all tests by Jest.

### `npm test:watch`

Runs the test watcher (Jest) in an interactive mode. By default, runs tests
related to files changed since the last commit.

### `npm run format`

Formats the codes with the Prettier's magic.

By default, you don't need to run
this command. Formatting code is handling by the `lint-staged` and husky on the
git pre-commit hook.
