export function map<T>(
  this: any,
  transform: (item: any, index: number, array: any[]) => T
) {
  let mapped = [];

  for (let index = 0; index < this.length; index++) {
    const element = this[index];
    mapped.push(transform(element, index, this));
  }

  return mapped;
}
