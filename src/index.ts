import { filter } from './filter';
import { map } from './map';
import { reduce } from './reduce';

(function() {
  Array.prototype.filter = filter;
  Array.prototype.map = map;
  Array.prototype.reduce = reduce;
})();
