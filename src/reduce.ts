export function reduce<T>(
  this: any,
  combine: (
    previousValue: any,
    currentValue: any,
    currentIndex: number,
    array: any[]
  ) => any,
  start: any = 0
) {
  let current = start;

  for (let index = 0; index < this.length; index++) {
    current = combine(current, this[index], index, this);
  }

  return current;
}
