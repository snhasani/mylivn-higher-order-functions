export function filter<T>(
  this: any,
  test: (item: any, index: number, array: any[]) => T
): T[] {
  let passed = [];

  for (let index = 0; index < this.length; index++) {
    const element = this[index];

    if (test(element, index, this)) {
      passed.push(element);
    }
  }

  return passed;
}
