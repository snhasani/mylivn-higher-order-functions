import { map } from '../src/map';

beforeEach(() => {
  Array.prototype.map = map;
});

describe('Array.prototype.map', () => {
  it('returns empty array', () => {
    const result = [].map((i) => i);

    expect(result).toBeTruthy();
    expect(result).toHaveLength(0);
  });

  it('the value of each item must be multiplied by 2 and returned', () => {
    const array = [0, 1, 2, 3];
    const result = array.map((n) => n * 2);

    expect(result).toBeTruthy();
    expect(result).toHaveLength(4);
    expect(result).toEqual([0, 2, 4, 6]);
  });

  it('the value of each item must be multiplied by its index and returned', () => {
    const array = [0, 2, 4];
    const result = array.map((n, index, array) => array[index] * index);

    expect(result).toBeTruthy();
    expect(result).toEqual([0, 2, 8]);
  });
});
