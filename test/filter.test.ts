import { filter } from '../src/filter';

beforeEach(() => {
  Array.prototype.filter = filter;
});

describe('Array.prototype.filter', () => {
  it('returns empty array', () => {
    const result = [].filter(Boolean);

    expect(result).toBeTruthy();
    expect(result).toHaveLength(0);
  });

  it('returns an array only true values', () => {
    const array = [1, [], {}, '', 0, null, undefined, -1, true, false];
    const result = array.filter(Boolean);

    expect(result).toBeTruthy();
    expect(result).toHaveLength(5);
  });

  it('returns numbers that are equal with itself index in the array', () => {
    const array = [0, 9, 8, 7, 6, 5];
    const result = array.filter((n, index, array) => array[index] === index);

    expect(result).toBeTruthy();
    expect(result).toEqual([0, 5]);
  });
});
