import { reduce } from '../src/reduce';

beforeEach(() => {
  Array.prototype.reduce = reduce;
});

describe('Array.prototype.reduce', () => {
  it('returns zero value for an empty array', () => {
    const result = [].reduce(() => 0 as never);

    expect(result).toBe(0);
  });

  it('returns sum of the numbers between one and 5', () => {
    const array = [1, 2, 3, 4, 5];
    const result = array.reduce((a, b) => a + b);

    expect(result).toEqual(15);
  });

  it('should start from 5 and decrus the index of the item from the current value', () => {
    const array = [0, 1, 2, 2];
    const result = array.reduce((a, b, index, array) => a - array[index], 5);

    expect(result).toEqual(0);
  });
});
